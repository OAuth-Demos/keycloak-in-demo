# keycloak-in-demo

How to make demo, examples, tutorials integrating Keycloak. 


## Import / Export 

Kind of backup/restore. The only drawback is that you have to restart the server (about 7s).

### Console quirks

Note: the *secrets* are *not* exported via the [Admin Console export](https://www.keycloak.org/docs/latest/server_admin/index.html#admin-console-export-import)!

>IF exported via the console, the JSON file contains `"secret" : "**********",` AND it will be imported as-is! You *HAVE TO* [Regenerate secret](https://www.keycloak.org/docs/latest/server_admin/index.html#_client-credentials) to get a new value.
> It is *not* automatic! 

Import is fine. You could also use the console to delete a realm. 

### Complete

Export everything: console admin, realms, users ...
```
bin/standalone.sh  -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.file=complete.json
```

Import (restore):
```
bin/standalone.sh  -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.strategy=OVERWRITE_EXISTING   -Dkeycloak.migration.file=Complete.json  
```

> Note: It will recreate the Admin account.

> Note: in both cases, it does the export and keeps running

### Per realm

Export all the realms in `MyRealms/`:
```
bin/standalone.sh -Dkeycloak.migration.action=export -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
    -Dkeycloak.migration.provider=dir -Dkeycloak.migration.dir=MyRealms
```


Copy the realms you want in a folder `MyRealmsOK/` 
```
bin/standalone.sh -Dkeycloak.migration.action=import -Dkeycloak.migration.strategy=OVERWRITE_EXISTING \
    -Dkeycloak.migration.provider=dir -Dkeycloak.migration.dir=MyRealmsOK
```

> If you rename the files don't forget the naming convention: `<REALM_NAME>-realm.json`.

#### Alternatives

I mention them for completness, you can skip this chapter!  The previous method is versatile and reliable.

You could also export just *one* realm:
```
bin/standalone.sh  -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.realmName=Demo  -Dkeycloak.migration.file=demo.json
```
> beware it is case sensitive and the console always displays the first letter uppercase!!! Very error prone. (You get an NPE if the realm does not exist.)


And there are many options to import: 

Import multiple files (*no overwrite!* if a realm already exists, just output an `info` - not even a `warning` - and continue on):
```
bin/standalone.sh  -Dkeycloak.import=./demo-1.json,./demo-3.json
```

Import with overwrite:
```
bin/standalone.sh  -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile \
    -Dkeycloak.migration.file=MyRealms/demo.json
```

## Scripts & Strategies

Overall it is quite convenient to be able to export/import everything, or just a realm.

- You can provide one json file with your complete setup and launch Keycloak with the import option in one go.
    - Dead easy
    - the user can screw up things and just relaunch to get the original state.
    - the secrets configured in application does not have to be changed
    - The file is in clear text and one can search for secrets, names or IDs.
    - "Container like" experience
- You can provide a modular setup, create the admin, import realms individualy ...
    - the user can tweak the script, easily change the admin password
    - experiment with various realms in parallele

It allows to build reliable examples quickly. 


The "one file" example is just the import line from the previous chapter.

For a modular setup, we do not import the *Master* realm. You eport the realms in a folder, keep what is needed and import the folder.

Example for a modular setup:
```
#!/usr/bin/sh
# Setup and launch Keycloak.

KEYCLOACK_HOME=~/.../oauth-test/keycloak-4.4.0.Final
REALMS_DIR=~/.../oauth-test/src/test/resources/keycloak-realms/

# Can be run many times
${KEYCLOACK_HOME}/bin/add-user-keycloak.sh -u kadmin -p kadmin

${KEYCLOACK_HOME}/bin/standalone.sh -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=dir \
    -Dkeycloak.migration.strategy=OVERWRITE_EXISTING -Dkeycloak.migration.dir=${REALMS_DIR}
```

## Keycloak on the Command line

There are multiple ways to use the command-line with Keycloak.

- `kcadm`, the [Admin CLI command-line tool](https://www.keycloak.org/docs/latest/server_admin/index.html#the-admin-cli) very versatile, but might be more suited for interactive access than scripting.
- Command line arguments to `bin/standalone.sh`
    - example: [import/export](https://www.keycloak.org/docs/latest/server_admin/index.html#_export_import) are very convenient.
- There are very handy scripts, like `bin/add-user-keycloak.sh` related to [Server Initialisation](https://www.keycloak.org/docs/latest/server_admin/index.html#server-initialization)


## Docker, containers

Not really required, as the standalone install is trivial.

- https://github.com/jboss-dockerfiles/keycloak/blob/master/server/Dockerfile
- https://store.docker.com/community/images/jboss/keycloak-examples/dockerfile
- https://hub.docker.com/r/jboss/keycloak/~/dockerfile/
- https://github.com/dfranssen/docker-keycloak-import-realm/blob/master/Dockerfile



